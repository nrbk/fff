package com.codistan.java.june;

public class June02_Assignment3 {

	public static void main(String[] args) {

		int startNum = 200;
		int finishNum = 0;
		int step = 5;
		int tempInt;

		System.out.println("Printing using FOR loop: ");
		for (int i = startNum; i >= finishNum; i -= step) {
			System.out.print(i + ", ");
		}

		System.out.println("\r\rPrinting using WHILE loop: ");
		tempInt = startNum;
		while (tempInt >= finishNum) {
			System.out.print(tempInt + ", ");
			tempInt -= step;
		}

		System.out.println("\r\rPrinting using DO WHILE loop: ");
		tempInt = startNum;
		do {
			System.out.print(tempInt + ", ");
			tempInt -= step;
		}
		while (tempInt >= finishNum);
	}

}
