package com.codistan.java.june;

public class June02_Assignment6 {
	
	public static void main(String[] args) {
		
		int a = 100;
		int b = 0;
		
		while (a >= b) {
			System.out.print(a + ", " + b + ", ");			
			a = a -5;
			b = b + 5;			
		}
		
	}

}
