package com.codistan.java.june;

import java.util.Scanner;

public class June02_Assignment2 {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		System.out.print("Enter the number to count by: ");
		String userInput = myScanner.nextLine();
		Integer userInt = Integer.parseInt(userInput.trim());
		int sum = 0;
		int counter = 0;

		while (userInt < 0 | userInt > 100) {
			System.out.print("Please enter a number between 0 and 100: ");
			userInput = myScanner.nextLine();
			userInt = Integer.parseInt(userInput.trim());
		}

		loop: while (sum < 100) {
			if ((sum + userInt) > 100)
				break loop;
			sum += userInt;
			System.out.print(sum + ", ");
			counter++;
		}

		System.out.println(
				"\rIt took " + counter + " times for loop to count from 0 to 100 using your step number of " + userInt);
	}

}
