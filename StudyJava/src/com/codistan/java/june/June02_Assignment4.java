package com.codistan.java.june;

import java.util.Scanner;

public class June02_Assignment4 {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		System.out.print("Enter your string you'd like to reverse: ");
		String userInput = myScanner.nextLine().trim();
		//userInput = "Hello World this is QA class and we are doing great!";

		for (int i = userInput.length() - 1; i >= 0; i--)
			System.out.print(userInput.charAt(i) + "");

	}

}
