package com.codistan.java.june;

import java.util.Scanner;

public class June02_Assignment1 {
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Please enter student name: ");
		String studentName = myScanner.nextLine();
		System.out.print("Please enter sutdent grade: ");
		int grade = myScanner.nextInt();
		String gradeLetter;
		
		if (grade < 0 || grade > 100) gradeLetter = "invalid";
		else if(grade > 85) gradeLetter = "A";
		else if (grade <= 85 & grade > 75) gradeLetter = "B";
		else if (grade <= 75 & grade > 65) gradeLetter = "C";
		else if (grade <= 65 & grade > 55) gradeLetter = "D";
		else gradeLetter = "F";

		System.out.println(studentName + "'s grade is " + gradeLetter);
		
	}

}
