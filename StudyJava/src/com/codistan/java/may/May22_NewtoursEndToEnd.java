package com.codistan.java.may;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class May22_NewtoursEndToEnd {
	
	public static void main(String[] args) {
		
	System.setProperty("webdriver.chrome.driver", "c:\\Webdriver\\chromedriver.exe");
	WebDriver surucu = new ChromeDriver();
	
	surucu.manage().window().maximize();	
	String url = new String("http://newtours.demoaut.com/");
	//url = "http://newtours.demoaut.com/mercuryreservation.php"; // Going straight to second page
	surucu.get(url);
	
	// **** LOGIN page ****
	
	String userName = new String("tutorial");
	String password = new String("tutorial");
	String urlFlightPage = new String("http://newtours.demoaut.com/mercuryreservation.php");
	
	WebElement elUsername = surucu.findElement(By.xpath("//input[@name='userName']"));
	WebElement elPassword = surucu.findElement(By.xpath("//input[@type='password']"));
	WebElement elSubmit = surucu.findElement(By.xpath("//input[@name='login']"));
	
		
	elUsername.sendKeys(userName);
	elPassword.sendKeys(password);
	elSubmit.submit();

	String currentURL = new String(surucu.getCurrentUrl());
	
	if (urlFlightPage.equals(currentURL)) {
		System.out.println("Expected and current URLs match");
	} else {
		if (currentURL.contains(urlFlightPage)) {
			System.out.println("Current URL contains " + urlFlightPage);
		}
	}
	System.out.println("Current URL: " + currentURL);
	
	
	// **** FLIGHT FINDER page ****
	
	WebElement elRadioRoundTrip = surucu.findElement(By.xpath("//input[@name='tripType'][@value='roundtrip']"));
	WebElement elRadioOneWay = surucu.findElement(By.xpath("//input[@value='oneway']"));
	WebElement elPassengerCount = surucu.findElement(By.xpath("//select[@name='passCount']"));	
	WebElement elDepartFrom = surucu.findElement(By.xpath("//select[@name='fromPort']"));
	WebElement elDepartMonth = surucu.findElement(By.xpath("//select[@name='fromMonth']"));
	WebElement elDepartDay = surucu.findElement(By.xpath("//select[@name='fromDay']"));
	WebElement elArriveIn = surucu.findElement(By.xpath("//select[@name='toPort']"));
	WebElement elReturnMonth = surucu.findElement(By.xpath("//select[@name='toMonth']"));
	WebElement elReturnDay = surucu.findElement(By.xpath("//select[@name='toDay']"));
	WebElement elRadioFlightClass = surucu.findElement(By.xpath("//input[@name='servClass'][@value='Business']"));
	WebElement elAirline = surucu.findElement(By.xpath("//select[@name='airline']"));
	WebElement elSubmitContinue = surucu.findElement(By.xpath("//input[@name='findFlights']"));
	
	Select selPassengerCount = new Select(elPassengerCount);
	Select selDepartFrom = new Select(elDepartFrom);
	Select selDepartMonth = new Select(elDepartMonth);
	Select selDepartDay = new Select(elDepartDay);
	Select selArriveIn = new Select(elArriveIn);
	Select selReturnMonth = new Select(elReturnMonth);
	Select selReturnDay = new Select(elReturnDay);
	Select selAirline = new Select(elAirline);
	
	elRadioRoundTrip.click();
	selPassengerCount.selectByValue("2");
	selDepartFrom.selectByValue("Paris");
	selDepartMonth.selectByVisibleText("December");
	selDepartDay.selectByValue("24");
	selArriveIn.selectByValue("Seattle");
	selReturnMonth.selectByVisibleText("December");
	selReturnDay.selectByValue("30");
	elRadioFlightClass.click();
	selAirline.selectByVisibleText("Unified Airlines");
	
	
	//**** Run verifications ****
	
	if (elRadioRoundTrip.isSelected()) {
		System.out.println(elRadioRoundTrip.getAttribute("value") + " is selected");
	}
	
	if (!elRadioOneWay.isSelected()) {
		System.out.println("Status of one way trip: " + elRadioOneWay.isSelected());
	}
	
	System.out.println("Number of passengers: " + selPassengerCount.getFirstSelectedOption().getText());
	System.out.println("Departure city: " + selDepartFrom.getFirstSelectedOption().getText());
	System.out.println("Departure month: " + selDepartMonth.getFirstSelectedOption().getText());
	System.out.println("Departure day: " + selDepartDay.getFirstSelectedOption().getText());
	System.out.println("Arrival city: " + selArriveIn.getFirstSelectedOption().getText());
	System.out.println("Return month: " + selReturnMonth.getFirstSelectedOption().getText());
	System.out.println("Return day: " + selReturnDay.getFirstSelectedOption().getText());
	System.out.println("Flight class: " + elRadioFlightClass.getAttribute("value"));
	elSubmitContinue.submit();
	
	// **** SELECT FLIGHT ****
	
	WebElement elDepartureFlightCode = surucu.findElement(By.xpath("//input[@value='Unified Airlines$363$281$11:24'][@name='outFlight']"));
	WebElement elReturnFlightCode = surucu.findElement(By.xpath("//input[@value='Unified Airlines$633$303$18:44'][@name='inFlight']"));
	WebElement elButtonReserve = surucu.findElement(By.xpath("//input[@name='reserveFlights']"));
	
	elDepartureFlightCode.click();
	elReturnFlightCode.click();
	elButtonReserve.click();
	
	// **** BOOK A FLIGHT ****
	
	WebElement elPax0FirstName = surucu.findElement(By.xpath("//input[@name='passFirst0']"));
	WebElement elPax0LastName = surucu.findElement(By.xpath("//input[@name='passLast0']"));
	WebElement elPax0Meal = surucu.findElement(By.xpath("//select[@name='pass.0.meal']"));
	Select selPax0Meal = new Select(elPax0Meal);
	WebElement elPax1FirstName = surucu.findElement(By.xpath("//input[@name='passFirst1']"));
	WebElement elPax1LastName = surucu.findElement(By.xpath("//input[@name='passLast1']"));
	WebElement elPax1Meal = surucu.findElement(By.xpath("//select[@name='pass.1.meal']"));
	Select selPax1Meal = new Select(elPax1Meal);	
	WebElement elCreditCardType = surucu.findElement(By.xpath("//select[@name='creditCard']"));
	Select selCreditCardType = new Select(elCreditCardType);
	WebElement elCreditCardNumber = surucu.findElement(By.xpath("//input[@name='creditnumber']"));
	WebElement elCreditCardFirstName = surucu.findElement(By.xpath("//input[@name='cc_frst_name']"));
	WebElement elCreditCardMiddleName = surucu.findElement(By.xpath("//input[@name='cc_mid_name']"));
	WebElement elCreditCardLastName = surucu.findElement(By.xpath("//input[@name='cc_last_name']"));
	WebElement elCreditCardExpirationMonth = surucu.findElement(By.xpath("//select[@name='cc_exp_dt_mn']"));
	Select selCreditCardExpirationMonth = new Select(elCreditCardExpirationMonth);
	WebElement elCreditCardExpirationYear = surucu.findElement(By.xpath("//select[@name='cc_exp_dt_yr']"));
	Select selCreditCardExpirationYear = new Select(elCreditCardExpirationYear);
	WebElement elBillingAddress1 = surucu.findElement(By.xpath("//input[@name='billAddress1']"));
	WebElement elBillingAddress2 = surucu.findElement(By.xpath("//input[@name='billAddress2']"));
	WebElement elBillingCity = surucu.findElement(By.xpath("//input[@name='billCity']"));
	WebElement elBillingState = surucu.findElement(By.xpath("//input[@name='billState']"));
	WebElement elBillingZip = surucu.findElement(By.xpath("//input[@name='billZip']"));
	WebElement elCheckboxTicketless = surucu.findElement(By.xpath("//*[contains(text(),'Ticketless Travel')]//preceding::input[1]"));
	WebElement elCheckboxSameAsBilling = surucu.findElement(By.xpath("//*[contains(text(),'Same as Billing Address')]//preceding::input[1]"));	
	Select selBillingCountry = new Select(surucu.findElement(By.xpath("//select[@name='billCountry']")));
	WebElement elButtonPurchase = surucu.findElement(By.xpath("//input[@name='buyFlights']"));
	
	elPax0FirstName.sendKeys("John");
	elPax0LastName.sendKeys("Smith");
	selPax0Meal.selectByValue("VGML");
	elPax1FirstName.sendKeys("Boris");
	elPax1LastName.sendKeys("Jackson");
	selPax1Meal.selectByValue("MOML");
	selCreditCardType.selectByValue("BA");
	elCreditCardNumber.sendKeys("4111111111111111");
	elCreditCardFirstName.sendKeys("John");
	elCreditCardLastName.sendKeys("Smith");
	selCreditCardExpirationMonth.selectByVisibleText("12");
	selCreditCardExpirationYear.selectByValue("2010");
	elBillingAddress1.clear();
	elBillingAddress1.sendKeys("7921 Jonas Branch dr");
	elBillingCity.clear();
	elBillingCity.sendKeys("McLean");
	elBillingState.clear();
	elBillingState.sendKeys("VA");
	elBillingZip.clear();
	elBillingZip.sendKeys("22102");
	elCheckboxSameAsBilling.click();
	
	// **** VERIFICATION ****
	
	System.out.println("1st passenger last name: " + elPax0FirstName.getAttribute("value"));
	System.out.println("1st passenger last name: " + elPax0LastName.getAttribute("value"));
	System.out.println("1st passenger meal: " + selPax0Meal.getFirstSelectedOption().getText());
	System.out.println("Credit card type: " + selCreditCardType.getFirstSelectedOption().getText());
	System.out.println("Credit card number: " + elCreditCardNumber.getAttribute("value"));
	System.out.println("Credit card expiry month: " + selCreditCardExpirationMonth.getFirstSelectedOption().getText().trim());
	System.out.println("Credit card expiry year: " + selCreditCardExpirationYear.getFirstSelectedOption().getText().trim());
	System.out.println("Ticketless travel: " + elCheckboxTicketless.isSelected());
	System.out.println("Billing address: " + elBillingAddress1.getAttribute("value"));
	System.out.println("Billing city: " + elBillingCity.getAttribute("value"));
	System.out.println("Billing state: " + elBillingState.getAttribute("value"));
	System.out.println("Billing Zip: " + elBillingZip.getAttribute("value"));
	System.out.println("Billing country: " + selBillingCountry.getFirstSelectedOption().getText().trim());
	
	elButtonPurchase.submit();
	
	// **** VERIFICATION ****
	if (surucu.getCurrentUrl().contains("http://newtours.demoaut.com/mercurypurchase2.php")) {
		System.out.println("URL verification: Passed");
		
	}
	if (surucu.getTitle().contains("Flight Confirmation: Mercury Tours")) {
		System.out.println("Page title verification: Passed");
	}
	
	String expectedConfirmationMessage = new String("Your itinerary has been booked!");
	String expectedTicketNumber = new String("2016-11-06064611");
	WebElement elConfirmMessage = surucu.findElement(By.xpath("//*[contains(text(),'Confirmation #')]"));
	String actualTicketNumber = new String(elConfirmMessage.getText().trim());
	String[] ticketParts = actualTicketNumber.split("#");
	String extractedTicket = ticketParts[1].trim();
	System.out.println("Expected ticket number: " + expectedTicketNumber);
	System.out.println("Actual ticket number: " + extractedTicket);
	
	int expectedTotalTaxes = 44;
	int expectedTotalPrice = 584;
	WebElement elTotalTaxes = surucu.findElement(By.xpath("//*[contains(text(),'Taxes')]/following::*[contains(text(),'USD')][1]"));
	WebElement elTotalPrice = surucu.findElement(By.xpath("//*[contains(text(),'Price')]/following::*[contains(text(),'USD')][1]"));
	String strActualTaxes = elTotalTaxes.getText();
	String strActualTotalPrice = elTotalPrice.getText();
	int actualTotalTaxes = Integer.parseInt(strActualTaxes.replaceAll("[^0-9]+", " ").trim());
	int actualTotalPrice = Integer.parseInt(strActualTotalPrice.replaceAll("[^0-9]+", " ").trim());
	WebElement elButtonBackToFlights = surucu.findElement(By.xpath("//img[@src='/images/forms/backtoflights.gif']"));
	WebElement elButtonBackToHome = surucu.findElement(By.xpath("//img[@src='/images/forms/home.gif']"));
	WebElement elButtonLogout = surucu.findElement(By.xpath("//img[@src='/images/forms/Logout.gif']"));

	System.out.println("Expected tax: $" + expectedTotalTaxes);
	System.out.println("Actual tax: $" + actualTotalTaxes);
	System.out.println("Expected total price: $" + expectedTotalPrice);
	System.out.println("Actual total price: $" + actualTotalPrice);
	
	System.out.println("Back To Flights button displayed? " + elButtonBackToFlights.isDisplayed());
	System.out.println("Back To Home button displayed? " + elButtonBackToHome.isDisplayed());
	System.out.println("Log Out button displayed? " + elButtonLogout.isDisplayed());
	
	//surucu.close();
	
	}
}
