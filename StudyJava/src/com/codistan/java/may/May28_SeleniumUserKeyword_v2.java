package com.codistan.java.may;

import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class May28_SeleniumUserKeyword_v2 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "c:\\webdriver\\chromedriver.exe");
		WebDriver drrv = new ChromeDriver();
		
		
		// **** WAITING FOR USER INPUT ****
		Scanner userScanner = new Scanner(System.in);
		System.out.print("Please enter your search query: ");
		String userInput = userScanner.nextLine();
		System.out.println("Please wait while system returns your results...");
		String searchEngineURL = new String("https://google.com");
		
		// **** SENDING USER QUERY TO SEARCH ENGINE ****
		drrv.get(searchEngineURL);
		WebElement searchTextbox = drrv.findElement(By.xpath("//input[@name='q']"));
		WebElement searchButton = drrv.findElement(By.xpath("//input[@name='btnK']"));
		searchTextbox.sendKeys(userInput);
		searchButton.submit();
		
		// **** SEARCH RESULTS ****
		WebElement elUrl = drrv.findElement(By.xpath("//cite[@class='iUh30']"));
		System.out.println(elUrl.getText());

		
		drrv.close();
		
		
		

		
	}

}
