package com.codistan.java.may;

import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class May28_SeleniumUserKeyword {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "c:\\webdriver\\chromedriver.exe");
		WebDriver drrv = new ChromeDriver();
		
		
		// **** WAITING FOR USER INPUT ****
		Scanner userScanner = new Scanner(System.in);
		System.out.print("Please enter your search query: ");
		String userInput = userScanner.nextLine();
		System.out.println("Please wait while system returns your results...");
		String searchEngineURL = new String("https://google.com");
		
		// **** SENDING USER QUERY TO SEARCH ENGINE ****
		drrv.get(searchEngineURL);
		WebElement searchTextbox = drrv.findElement(By.xpath("//input[@name='q']"));
		WebElement searchButton = drrv.findElement(By.xpath("//input[@name='btnK']"));
		searchTextbox.sendKeys(userInput);
		searchButton.submit();
		
		// **** SEARCH RESULTS ****
		String strSearchResultURLXpath = new String("(//div[@class='TbwUpd']//*[contains(text(),'http')])");
		WebElement searchResult = drrv.findElement(By.xpath(strSearchResultURLXpath));
		String[] arrXpath = new String[10];
		WebElement[] elResultURL = new WebElement[10];

		for (int i = 6; i < arrXpath.length; i++) {
			arrXpath[i] = strSearchResultURLXpath + "[" + i + "]";
			elResultURL[i] = drrv.findElement((By.xpath(arrXpath[i])));
			System.out.println(elResultURL[i].getText());
		}
		
		drrv.close();
		
		
		

		
	}

}
