package com.codistan.java.may;

import java.util.Scanner;

public class May25_CountCharactersInString {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		String target = myScanner.nextLine();
		String inputChar = myScanner.nextLine();
		
		char search = inputChar.charAt(0);
		int total = 0;

		for (int i = 0; i < target.length(); i++) {
			if (target.charAt(i) == search) {
				total++;
			}
		}
		if (total == 0) {
			System.out.println("\"" + search + "\" has not been found.");
		} else {
			System.out.println("Total occurences of \"" + search + "\": " + total);
		}
	}

}
