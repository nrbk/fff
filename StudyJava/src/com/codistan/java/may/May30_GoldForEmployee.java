package com.codistan.java.may;

import java.util.Scanner;

public class May30_GoldForEmployee {

	public static void main(String[] args) {

		// *** USER INPUT ***

		Scanner userScanner = new Scanner(System.in);
		System.out.print("Is employee a manager (y/n)? ");
		String strManager = new String(userScanner.nextLine());
		System.out.print("Enter number of years of employment: ");
		String strNumOfYears = new String(userScanner.nextLine());

		// *** DATA OPTIMIZATION ***

		int intYearsEmployed = Integer.parseInt(strNumOfYears.trim());
		strManager = strManager.toLowerCase().trim();
		boolean isManager;
		if (strManager.equals("yes") || strManager.equals("y"))
			isManager = true;
		else
			isManager = false;

		// *** FINAL CALCULATIONS ***

		int totalGold = 0; // Amount of total gold earned throughout career
		int singlePayment = 0;

		if (isManager) {
			singlePayment = 3;
			// **** Manager ****
			if (intYearsEmployed >= 20)
				totalGold = singlePayment * ((intYearsEmployed / 3) * 2) + singlePayment * ((intYearsEmployed / 10) * 2) - 21;
			else

				totalGold = singlePayment * (intYearsEmployed / 3 + intYearsEmployed / 10);
		} else {

			// **** Worker ****
			if (intYearsEmployed >= 20)
				totalGold = (intYearsEmployed / 3) * 2 + (intYearsEmployed / 10) * 2 - 7;
			else
				totalGold = intYearsEmployed / 3 + intYearsEmployed / 10;
		}

		System.out.println("\rTotal gold received during " + intYearsEmployed + " years of employment: " + totalGold);
	}

}
