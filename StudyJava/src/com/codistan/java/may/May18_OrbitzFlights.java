package com.codistan.java.may;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class May18_OrbitzFlights {
	
	public static void main(String[] args) {
		
		//Declaring Chrome driver 
		//System.setProperty("webdriver.chrome.driver", "c:\\Webdriver\\chromedriver.exe");
		//WebDriver drv = new ChromeDriver();	
		
		//Declaring Gecko driver 
		System.setProperty("webdriver.gecko.driver", "c:\\Webdriver\\geckodriver.exe");
		WebDriver drv = new FirefoxDriver();	
		
		//Launching target website
		drv.get("https://www.orbitz.com");
		drv.manage().window().maximize();
		
		// ***** Declaration of WebElements *****
		WebElement elemTabFlights = drv.findElement(By.xpath("//span[@class='icon icon-flights']"));
		WebElement elemAdults = drv.findElement(By.xpath("//select[@id='flight-adults-hp-flight']"));
		WebElement elemChildren = drv.findElement(By.xpath("//select[@id='flight-children-hp-flight']"));
		WebElement elemChildOneAge = drv.findElement(By.xpath("//select[@id='flight-age-select-1-hp-flight']"));
		WebElement elemChildTwoAge = drv.findElement(By.xpath("//select[@id='flight-age-select-2-hp-flight']"));
		WebElement elemChildThreeAge = drv.findElement(By.xpath("//select[@id='flight-age-select-3-hp-flight']"));
		WebElement elemDateOfDeparture = drv.findElement(By.xpath("//input[@id='flight-departing-hp-flight']"));
		WebElement elemDateOfReturn = drv.findElement(By.xpath("//input[@id='flight-returning-hp-flight']"));
		WebElement elemFlyingFrom = drv.findElement(By.xpath("//input[@id='flight-origin-hp-flight']"));
		WebElement elemFlyingTo = drv.findElement(By.xpath("//input[@id='flight-destination-hp-flight']"));
		WebElement elemSearchButton = drv.findElement(By.xpath("//span[contains(text(),'Search') and @class='btn-label']"));
		
		//WebElement elemPickDateOfDeparture = drv.findElement(By.xpath("//button[@class='datepicker-cal-date' and @data-year='2019' and @data-month='5' and @data-day='12']"));
		
		Select selAdults = new Select(elemAdults);
		Select selChildren = new Select(elemChildren);
		Select selChildOneAge = new Select(elemChildOneAge);
		Select selChildTwoAge = new Select(elemChildTwoAge);
		Select selChildThreeAge = new Select(elemChildThreeAge);
		
		// ***** Declaration of data *****
		String strFlyingFrom = "Chicago, IL (ORD-O'Hare Intl.)";
		String strFlyingTo = "Istanbul, Turkey (IST)";
		String strDepartureDate = "09/10/2019";
		String strReturnDate = "09/22/2019";
		
		elemTabFlights.click();
		selAdults.selectByVisibleText("2");
		selChildren.selectByVisibleText("3");
		selChildOneAge.selectByValue("7");
		selChildTwoAge.selectByValue("5");
		selChildThreeAge.selectByValue("2");
		elemFlyingFrom.sendKeys(strFlyingFrom + Keys.TAB);
		elemFlyingTo.sendKeys(strFlyingTo + Keys.TAB);
		elemDateOfDeparture.sendKeys(strDepartureDate + Keys.TAB);
		elemDateOfReturn.sendKeys(Keys.CONTROL, "a");
		elemDateOfReturn.sendKeys(Keys.BACK_SPACE);
		elemDateOfReturn.sendKeys(strReturnDate);
		elemSearchButton.submit();

	}

}
